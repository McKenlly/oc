
#include <dlfcn.h>
#include <stdlib.h>
#include <stdio.h>
#include "usr/vector.h"
const int SIZE_STRING = 256;

int main(int argc, char *argv[])
{
    void* libHandle;
    void* (*vector_create)(size_t count, size_t size);
    void (*vector_at_set)(const Vector * restrict vector, 
                                size_t i, const void * restrict source, 
                                        size_t cell_count);
    void (*vector_at_get)(const Vector * restrict vector, 
                            size_t i, void * restrict dest, size_t cell_count);
    char* err;
    
    libHandle = dlopen("./libvector.so", RTLD_LAZY);
    if (libHandle == NULL) {
        fprintf(stderr, "%s\n", dlerror());
        return 0;
    }
    printf("Create\n");
    vector_create = dlsym(libHandle, "vector_create");
    if ((err = dlerror()) != NULL) {
        fprintf(stderr, "%s\n", err);
        return 0;
    }


    printf("insert\n");
    vector_at_set = dlsym(libHandle, "vector_at_set");
    if ((err = dlerror()) != NULL) {
        fprintf(stderr, "%s\n", err);
        return 0;
    }
    printf("GET\n");
    vector_at_get = dlsym(libHandle, "vector_at_get");
    if ((err = dlerror()) != NULL) {
        fprintf(stderr, "%s\n", err);
        return 0;
    }
    Vector *vector = (*vector_create)(10, sizeof(long double));
    long double temp = 0.0l;
    for (size_t i = 0u; i < 10; ++i)
    {
        scanf("%Lf", &temp);
        (*vector_at_set)(vector, i, &temp, 1u);
    }
    putchar('\n');
    for (size_t i = 0u; i < 10; ++i)
    {
        (*vector_at_get)(vector, i, &temp, 1u);
        printf("%Lf\n", temp);
    }
    return 0;
} 
