#include <iostream>
#include <dlfcn.h>
#include "usr/vector.h"
const int SIZE_STRING = 256;
/*
void* load_library(const char* library_so_path)
{

    // load the triangle library
    void* dll_ptr = dlopen(library_so_path, RTLD_LAZY);
    if (dll_ptr == NULL)
    {
        printf("ERROR: load_dynamic_library, cannot load library %s, error= %s \n",
               library_so_path, dlerror() );
        //cerr << "Cannot load library: " << dlerror() << '\n';
        return NULL;
    }
    printf("OK: library is load\n");
    // reset errors
    dlerror();

    return dll_ptr;
}

void* load_symbol(void* loaded_library, const char* symbol_name)
{
    dlerror(); // reset errors

    void* symbol_ptr = dlsym(loaded_library, symbol_name);
    const char *dlsym_error = dlerror();
    if (dlsym_error)
    {
        printf("ERROR: load_symbol, cannot load symbol %s, error= %s \n",
               symbol_name, dlsym_error);
        dlclose(loaded_library);
        return NULL;
    }

    return symbol_ptr;
}
*/

int main(int argc, char *argv[])
{
    size_t n = 10;
    Vector *vector = vector_create(n, sizeof(long double));

    long double temp = 0.0l;
    for (size_t i = 0u; i < n; ++i)
    {
        scanf("%Lf", &temp);
        vector_at_set(vector, i, &temp, 1u);
    }

    putchar('\n');
//	vector_resize(vector, n + 20);

    for (size_t i = 0u; i < vector_size(vector); ++i)
    {
        vector_at_get(vector, i, &temp, 1u);
        printf("%Lf\n", temp);
    }

    vector_destroy(vector);
    vector = NULL;
    return 0;
}