
#include <dlfcn.h>
#include <stdlib.h>
#include <stdio.h>
#include "usr/vector.h"
const int SIZE_STRING = 256;

int main(int argc, char *argv[])
{
    int i;
    Vector *vector = vector_create(10, sizeof(long double));
    scanf("%d", &i);
    vector_at_set(vector, 12, &i, 1u);
    return 0;
} 
