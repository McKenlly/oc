#include <csignal>
#include <cstdio>
#include <unistd.h>
#include <cstdlib>
static void sigint_handler(int signo) {
    printf("Захвачен сигнал\n");
    exit(EXIT_SUCCESS);
}

int main() {
    if (signal(SIGSEGV, sigint_handler) == SIG_ERR) {
        printf("Невозможно обработать сигнал\n");
        exit(EXIT_FAILURE);
    }
    char string[256];
    int index = 0;
    char c;
    while (((c=getchar())&& c!='\n')) {
        //c = getchar();
        string[index++] = c;

    }
    string[index-1] = '\0';
    //printf("%s", string);
    return 0;
}