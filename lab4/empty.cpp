#include <iostream>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

void *func(void *arg) {
    pid_t *c = (pid_t *) arg;
    *c = getpid();
}
int main(int argc, char **argv) {
    pid_t x;
    pthread_t t;
    pthread_create(&t, NULL, func, &x);
    pthread_join(t, NULL);
    int desk;
    char fileName[] = "/home/box/main.pid";
    /*if (access(fileName, F_OK) == 0) {
        desk = open(fileName, O_APPEND | O_RDWR);
    } else {
        desk = open(fileName, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
    }*/
    FILE *f = fopen(fileName, "w+");
    size_t size = 1;
    fprintf(f, "%d", x);
    //write(desk, &x, 1);
    fclose(f);
    //close(desk);
    return 0;
}