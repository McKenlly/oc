//
// Created by bokoch on 11.11.17.
//
#include <sys/mman.h>
#include <cstdio>
#include <iostream>
#include <memory>
#include <cstring>
#include <fcntl.h>
#include <assert.h>
#include <zconf.h>
#include <sys/types.h>
#include <sys/io.h>
#include <sys/file.h>
#include "TextProcessor.h"

void ParseFlags(const int &, char (**line), Options &);
void Display(void);
void PrintCommandsHelp(void);
void CheckFile(Options &);
void EnterLine(char (&line)[MAX_LENGTH_FILE][MAX_LENGTH_FILE]);
void PrintPartText(Options &, int, int);

int main(int argc, char *argv[]) {
    Options opt;
    ParseFlags(argc, argv, opt);
    CheckFile(opt);
    char commandLine[MAX_LENGTH_FILE][MAX_LENGTH_FILE];
    do {
        EnterLine(commandLine);
        if (strcmp(commandLine[0], "print") == 0) {
            PrintPartText(opt, atoi(commandLine[1]), atoi(commandLine[2]));
            continue;
        }
        if (strcmp(commandLine[0], "replace") == 0) {
                ReplaceText(opt);
                printf("OK\n");
                continue;
        }
        if (strcmp(commandLine[0], "find") == 0) {
            FindSubString(opt);
            continue;
        }
        if (strcmp(commandLine[0], "clear") == 0) {
            system("clear");
            continue;
        }
        if (strcmp(commandLine[0], "help") == 0) {
            PrintCommandsHelp();
            continue;
        }
        if (strcmp(commandLine[0], "stats") == 0) {
            ShowStats(opt);
        }
        if (strcmp(commandLine[0], "serialize") == 0) {
            if (flock(opt.fd, LOCK_UN)) {
                handle_error("flock unlock");
            }
            close(opt.fd);
            opt.opened = false;
            for (int i = 0; i < 256; i++) opt.fileName[i] = '\0';
            strcpy(opt.fileName, commandLine[1]);
            CheckFile(opt);
        }

    } while(strcmp(commandLine[0], "quit") != 0);

    if (opt.opened) {

        if (flock(opt.fd, LOCK_UN)) {
            handle_error("flock unlock");
        }
        opt.opened = false;
        close(opt.fd);
    }
    return 0;
}
