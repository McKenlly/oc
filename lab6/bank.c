#include "bank.h"

ClientDB ClientDBCreate(void)
{
    ClientDB cDB = (ClientDB) malloc(sizeof(*cDB));
    if (!cDB) {
        fprintf(stderr, "ERROR: no memory\n");
        exit(NOT_MEMORY);
    }
    cDB->persons = (Client) malloc(sizeof(*(cDB->persons)));
    cDB->size = 0;
    cDB->freespace = 1;
    return cDB;
}

void DBResize(ClientDB cDB)
{
    cDB->persons = realloc(cDB->persons, 2 * cDB->size * sizeof(*(cDB->persons)));
    if (!cDB->persons) {
        fprintf(stderr, "ERROR: no memory\n");
        exit(NOT_MEMORY);
    }
    cDB->freespace = cDB->size;
}

void ClientAdd(ClientDB cDB, ID client)
{
    if (!cDB->freespace) {
        DBResize(cDB);
    }

    cDB->persons[cDB->size].client = client;
    cDB->persons[cDB->size].money = 0;
    cDB->size++;
    cDB->freespace--;
}

void ClientDBPrint(ClientDB cDB)
{
    if (cDB) {
        for (uint32_t i = 0; i < cDB->size; ++i) {
            printf("ID: %d\t", cDB->persons[i].client);
            printf("money: %d\n", cDB->persons[i].money);
        }
    }
}

Client ClientFind(ClientDB cDB, int clientint)
{
    if (cDB) {
        for (uint32_t i = 0; i < cDB->size; ++i) {
            if (cDB->persons[i].client == clientint) {
                return &(cDB->persons[i]);
            }
        }
    }
    return NULL;
}

void ClientDBDestroy(ClientDB *cDB)
{
    free((*cDB)->persons);
    (*cDB)->persons = NULL;
    free(*cDB);
    *cDB = NULL;
}

void ClientAccIncrease(Client cl, int money)
{
    cl->money += money;
}

void ClientAccDecrease(Client cl, int money)
{
    cl->money -= money;
}

int ClientHasEnoughMoney(Client cl, int money)
{
    return cl->money >= money;
}

void MoneyPut(int clientint, int money, ClientDB cDB)
{
    Client cl = ClientFind(cDB, clientint);

    if (cl) {
        ClientAccIncrease(cl, money);
    } else {
        ClientAdd(cDB, clientint);
        cl = ClientFind(cDB, clientint);
        ClientAccIncrease(cl, money);
    }
}

int MoneyGet(int clientint, int money, ClientDB cDB)
{
    Client cl = ClientFind(cDB, clientint);
    if (!cl) {
        return NOT_CLIENT;
    }
    if (ClientHasEnoughMoney(cl, money)) {
        ClientAccDecrease(cl, money);
        return SUCCESS;
    } else {
        return NOT_ENOUGH_MONEY;
    }
}

int MoneySend(ID personsender, ID clientReceiver, int money, ClientDB cDB)
{
    Client clSender = ClientFind(cDB, personsender);
    if (!clSender) {
        return NOT_CLIENT;
    }
    Client clReceiver = ClientFind(cDB, clientReceiver);
    if (!clReceiver) {
        return RECEIVER_NOT_CLIENT;
    }

    if (ClientHasEnoughMoney(clSender, money)) {
        ClientAccDecrease(clSender, money);
        ClientAccIncrease(clReceiver, money);
        return SUCCESS;
    } else {
        return NOT_ENOUGH_MONEY;
    }
}

int CheckAccount(ID client, ClientDB cDB)
{
    Client cl = ClientFind(cDB, client);
    if (!cl) {
        return NOT_CLIENT;
    }
    return cl->money;
}

