#ifndef _BANK_H_
#define _BANK_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>

#define SUCCESS 1
#define NOT_MEMORY -1
#define NOT_ENOUGH_MONEY -2
#define NOT_CLIENT -3
#define RECEIVER_NOT_CLIENT -4

typedef int32_t ID;

typedef struct _client {
    ID client;
    int money;
} *Client;

typedef struct _clientDB {
    Client persons;
    uint32_t size;//Количество клиентов
    uint32_t freespace;//свободное пространство
} *ClientDB;//Lvalue

ClientDB ClientDBCreate(void);
void ClientAdd(ClientDB cDB, ID client);
void ClientDBPrint(ClientDB cDB);
Client ClientFind(ClientDB cDB, int clientint);
void ClientDBDestroy(ClientDB *cDB);

void ClientAccIncrease(Client cl, int money);
void ClientAccDecrease(Client cl, int money);
int ClientHasEnoughMoney(Client cl, int money);
void MoneyPut(int Clientint, int money, ClientDB cDB);
int MoneyGet(int Clientint, int money, ClientDB cDB);
int MoneySend(ID clientSender, ID clientReceiver, int money, ClientDB cDB);
int CheckAccount(ID client, ClientDB cDB);

#endif