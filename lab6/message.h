#ifndef _MESSAGE_H_
#define _MESSAGE_H_

#include <inttypes.h>

#define STR_SIZE 256

typedef int32_t ID;

typedef struct _msg {
    ID client; //ID клиента
    int money;
    int action; 
    ID receiverClient; //ID
    void *requester; // socket
    char message[STR_SIZE];
} MsgData;

#endif