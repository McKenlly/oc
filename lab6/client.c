#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <string.h>
#include <pthread.h>
#include <stdbool.h>
#include "zmq.h"
#include "message.h"

void menuUser();
void *SendRecv(void *arg);

int main(int argc, char **argv)
{
    void *context = zmq_ctx_new();

    ID client, bank;
    if (argc == 2 && !strcmp(argv[1], "client"))
    {
        client = atoi(argv[1]);
    }
    else
    {
        printf("Enter client's login: ");
        scanf("%d", &client);
    }
    char adress[STR_SIZE];
    printf("Enter bank's adress: ");
    scanf("%d", &bank);
    sprintf(adress, "%s%d", "tcp://localhost:", bank);
    printf("tcp://localhost:%d \n", bank);
    void *sendSocket = zmq_socket(context, ZMQ_REQ); //создает сокет из контекста
    zmq_connect(sendSocket, adress);                 //подключение сокета к порту address

    int act = 0;
    int money = 0;

    do {
        menuUser();
        scanf("%d", &act);
        system("clear");
        MsgData md;
        md.action = act;
        md.client = client;
        switch (act) {
        case 0: 
        {
            pthread_t th;
            md.requester = sendSocket;
            pthread_create(&th, NULL, SendRecv, &md);
            pthread_detach(th);

            break;
        }
        case 1:
        {
            pthread_t th;
            md.requester = sendSocket;
            pthread_create(&th, NULL, SendRecv, &md);
            pthread_detach(th);

            break;
        }
        //Put money
        case 2: {
            printf("Enter the money: ");
            scanf("%d", &money);

            md.money = money;
            pthread_t th;
            md.requester = sendSocket;
            pthread_create(&th, NULL, SendRecv, &md);
            pthread_detach(th);
            break;
        }
        //Get money
        case 3: {
            printf("Enter the money: ");
            scanf("%d", &money);
            md.money = money;
            pthread_t th;
            md.requester = sendSocket;
            pthread_create(&th, NULL, SendRecv, &md);
            pthread_detach(th);
            break;
        }
        //Send money
        case 4:
        {
            int receiverClient;
            printf("Enter receiver id: ");
            scanf("%d", &receiverClient);

            printf("Enter the money: ");
            scanf("%d", &money);

            md.money = money;
            md.receiverClient = receiverClient;

            pthread_t th;
            md.requester = sendSocket;
            pthread_create(&th, NULL, SendRecv, &md);
            pthread_detach(th);

            break;
        }
        //Check balance
        case 5:
        {
            pthread_t th;
            md.requester = sendSocket;
            pthread_create(&th, NULL, SendRecv, &md);
            pthread_detach(th);

            break;
        }
            //leave the bank
        case 6:
        {
            printf("Exit\n");
            break;
        }
        default:
        {
            printf("Inccorect command\n");
            break;
        }
        }
    } while (act != 6);
    zmq_close(sendSocket);
    zmq_ctx_destroy(context);

    return 0;
}


void menuUser()
{
    printf("0) Run server\n");
    printf("1) Stop server\n");
    printf("2) Put money\n");
    printf("3) Get money\n");
    printf("4) Send money to another person\n");
    printf("5) Check balance\n");
    printf("6) Exit\n");
}
void *SendRecv(void *arg)
{
    MsgData *md = (MsgData *)arg;
    zmq_msg_t message;
    zmq_msg_init_size(&message, sizeof(MsgData));
    memcpy(zmq_msg_data(&message), md, sizeof(MsgData));
    zmq_msg_send(&message, md->requester, 0);
    zmq_msg_close(&message);
    zmq_msg_init(&message);
    zmq_msg_recv(&message, md->requester, 0);
    md = (MsgData *)zmq_msg_data(&message);
    printf("%s\n", md->message);
    zmq_msg_close(&message);
    pthread_exit(NULL);
    return 0;
}