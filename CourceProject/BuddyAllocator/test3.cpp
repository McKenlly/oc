#include "BuddyAllocator.hpp"
#include <ctime>
#include <iostream>
#include <chrono>
int main() {

    std::vector<void*> allocations_(30000);

    BuddyAllocator ba;
    ba.initialize(30, 4);
    void* alloc = nullptr;

    for (int i = 0; i < 300; i++) {
        alloc = ba.allocate(1); 
        allocations_[i] = alloc;
    }
    auto start_time = std::chrono::steady_clock::now();
    for (int i = 0; i < 300; i++) {
        ba.deallocate(allocations_[i]); 
        auto end_time = std::chrono::steady_clock::now();
        auto elapsed_ns = std::chrono::duration_cast<std::chrono::nanoseconds>(end_time - start_time);
        std::cout << elapsed_ns.count() << std::endl;
    }
    return 0;
}
 
 
