#!/usr/bin/gnuplot -persist

set title "Time graph for allocation blocks different size"
set ylabel "time allocate one block"
set xlabel "counter allocate"
set terminal jpeg size 1024,960

set output "graph.jpg"
set style data linespoints
plot "OverRuntime" u  1:2 title "deallocateManyBytes", \
     "OverRuntime" u  1:3 title "deallocateOneBytes", \
     "OverRuntime" u  1:4 title "allocateManyBytes", \
     "OverRuntime" u  1:5 title "allocateOneBytes" 
