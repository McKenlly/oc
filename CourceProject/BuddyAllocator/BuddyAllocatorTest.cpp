#include "BuddyAllocator.hpp"
#include <ctime>
#include <iostream>
#include <chrono>
int main() {

    std::vector<void*> allocations_(30000);

    BuddyAllocator ba;
    ba.initialize(30, 4);
    void* alloc = nullptr;
    auto start_time = std::chrono::steady_clock::now();
    for (int i = 0; i < 30000; i++) {
        alloc = ba.allocate(1); 
        auto end_time = std::chrono::steady_clock::now();
        allocations_[i] = alloc;
        auto elapsed_ns = std::chrono::duration_cast<std::chrono::nanoseconds>(end_time - start_time);
        std::cout << elapsed_ns.count() << std::endl;
    }
    return 0;
}
