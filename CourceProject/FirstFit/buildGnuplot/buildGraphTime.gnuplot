#!/usr/bin/gnuplot -persist

set title "Time graph for Random deallocation 300 blocks of size many bytes"
set ylabel "time allocate one block"
set xlabel "counter allocate"
set terminal jpeg size 1024,960

set output "graph.jpg"
set style data linespoints
plot "OverRuntime" u  1:2 title "BuddyAllocator", \
     "OverRuntime" u  1:3 title "FirstFitAllocator"
     
