//
// Created by bokoch on 12/23/17.
//

#ifndef FIRSTFIT_FIRSTFITALLOCATOR_H
#define FIRSTFIT_FIRSTFITALLOCATOR_H
#include <cstdio>
#include <cstdlib>
#include <unordered_map>
#include <iostream>
#include <memory>

const unsigned int DEFAULT_SIZE_ALLOCATE = 122880000;
class FirstFitAllocator {
public:
    FirstFitAllocator() = default;
    void initialize(unsigned int size_alloc);
    void* allocate(unsigned int  n);
    void deallocate(void *p);
    struct Block {
//        Block(std::shared_ptr<Block> pointer_next, unsigned int is_size):
//                        next(pointer_next), size_block_(is_size){}
        std::shared_ptr<Block> next;
        void* data_;
        unsigned int size_block_;
    };
    ~FirstFitAllocator() {
        free(base_address_);
    }
protected:
    void Print(std::shared_ptr<Block> cur_pointer1);
    bool merge_blocks(std::shared_ptr<Block>);
    void listCurrRemove(std::shared_ptr<Block> b);
    void splitBlock(std::shared_ptr<Block> b, unsigned int size);
   // unsigned char is_availible(Block *current_block, unsigned int size);

private:
    void* base_address_;
    std::shared_ptr<Block> free_list_by_order_;
    std::unordered_map<void *, unsigned int> fixed_address_;
};


#endif //FIRSTFIT_FIRSTFITALLOCATOR_H
