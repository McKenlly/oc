cmake_minimum_required(VERSION 3.8)
project(FirstFit)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp FirstFitAllocator.cpp FirstFitAllocator.h)
add_executable(FirstFit ${SOURCE_FILES})