#include "FirstFitAllocator.h"


void FirstFitAllocator::initialize(unsigned int n) {
    if (n < DEFAULT_SIZE_ALLOCATE) {
        n = DEFAULT_SIZE_ALLOCATE;
    }
    base_address_ = malloc(n);
   // assert(base_address_);
    free_list_by_order_ = std::make_shared<Block>();
    free_list_by_order_->data_ = base_address_;
    free_list_by_order_->size_block_ = n;
   // std::cout << "Your bytes gets sucessfull" << std::endl;
}

void FirstFitAllocator::listCurrRemove(std::shared_ptr<Block> b) {
    std::shared_ptr<Block> p = free_list_by_order_;
    if (p == free_list_by_order_) {
        free_list_by_order_ = free_list_by_order_->next;
        return;
    }
    while (p->next) {
        if (p->next == b) {
            break;
        }
        p = p->next;
    }
    p->next = b->next;
}
void FirstFitAllocator::splitBlock(std::shared_ptr<Block> b, unsigned int size) {
    //Block* next_block = b->next;
    if (b->data_ == free_list_by_order_->data_) {
        std::shared_ptr<Block> next_block = free_list_by_order_->next;
//        std::cout << "Before:" << free_list_by_order_->data_ << std::endl;
        free_list_by_order_->data_ = (free_list_by_order_->data_ + size);
//        std::cout << "After:" << free_list_by_order_->data_ << std::endl;

        free_list_by_order_->next = next_block;
        free_list_by_order_->size_block_ -= size;
        return;
    }
    std::shared_ptr<Block> p = free_list_by_order_;
    while (p->next != b) {
        p = p->next;
    }
    std::shared_ptr<Block> new_block = std::make_shared<Block>();// = new Block;
    new_block->data_ = b->data_ + size;
    new_block->size_block_ = p->next->size_block_ - size;
    new_block->next = p->next->next;
    p->next = new_block;
}


void* FirstFitAllocator::allocate(unsigned int  n) {
    std::shared_ptr<Block> b = free_list_by_order_;
    while (b) {
        if (b->size_block_ >=  n) {
            // std::cout << "SIZECURBLOCK\t" << b->size_block_ << std::endl;
            if (b->size_block_ == n) {
            //    std::cout << "Allocate blocks == your size\n";
                fixed_address_[b->data_] = b->size_block_;
                std::shared_ptr<Block> tmp = b;
                listCurrRemove(b);
                // std::cout << "Pointer on start \t" << free_list_by_order_->data_ << std::endl;
                // std::cout << "Allocate " << tmp->data_ << std::endl;
                return tmp->data_;
            } else {
            //    std::cout << "Allocate blocks > your size. Spliting\n";
                std::shared_ptr<Block> tmp = std::make_shared<Block>();
                tmp->data_ = b->data_;
                tmp->size_block_ = b->size_block_;
                splitBlock(b, n);
                fixed_address_[tmp->data_] = n;
            //    std::cout << "Pointer on start \t" << free_list_by_order_->data_ << std::endl;
            //    std::cout << "Allocate " << tmp->data_ << std::endl;
                return tmp->data_;

            }
        }
        b = b->next;
    }
    throw std::runtime_error("Out of memory");
    return NULL;
}

void FirstFitAllocator::Print(std::shared_ptr<Block> cur_pointer1) {
    int  i =0;
    while (cur_pointer1) {
        //std::cout << "INDEX [" << i++ << "] = \t" << cur_pointer1->data_ << " \t" << cur_pointer1->size_block_<<std::endl;
        cur_pointer1 = cur_pointer1->next;
    }
}
bool FirstFitAllocator::merge_blocks(std::shared_ptr<Block> pointer) {
    std::shared_ptr<Block> cur_pointer = free_list_by_order_;
    std::shared_ptr<Block> prev_cur_pointer = NULL;

    while (cur_pointer) {
        if ((uintptr_t)cur_pointer->data_+cur_pointer->size_block_ == (uintptr_t)pointer->data_) {
            pointer->size_block_ += cur_pointer->size_block_;
            pointer->data_ = cur_pointer->data_;
            if (prev_cur_pointer) {
                prev_cur_pointer->next = cur_pointer->next;
            } else {
                free_list_by_order_ = free_list_by_order_->next;
            }
            break;
        }
        prev_cur_pointer = cur_pointer;
        cur_pointer = cur_pointer->next;
    }
    cur_pointer = free_list_by_order_;
    while (cur_pointer) {
        if ((uintptr_t)(cur_pointer->data_-pointer->size_block_) == (uintptr_t)(pointer->data_)) {
            cur_pointer->size_block_ += pointer->size_block_;
            cur_pointer->data_ = pointer->data_;
            break;
        }
        cur_pointer = cur_pointer->next;
    }
    if (!cur_pointer) {
        pointer->next = free_list_by_order_;
        free_list_by_order_ = pointer;
    }
    return true;
}
void FirstFitAllocator::deallocate(void *p) {
    auto it = fixed_address_.find(p);
    if (it != fixed_address_.end()) {
        std::shared_ptr<Block> new_node = std::make_shared<Block>();
        new_node->data_ = it->first;
        new_node->size_block_ = it->second;
        merge_blocks(new_node);
        fixed_address_.erase(it);
       std::cout << "deallocate\t" << new_node->size_block_ << "bytes\tat address " << new_node->data_ << std::endl;
    }  else {
        throw std::runtime_error("Invalid Free: No much pointer");
    }
}